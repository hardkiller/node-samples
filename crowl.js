
var fetch = require('node-fetch');
var fs = require('fs');


var maxThreads = 100;

var options = {
    url: 'http://api.tiles.mapbox.com/v4/mapbox.satellite',
    minLevel: 6,
    maxLevel: 10,
    token: '?access_token=pk.eyJ1IjoiaGFyZGtpbGxlciIsImEiOiJjaW85cWwwaGEwMDd6dnJseTJxMHloNDNrIn0.GeRLRFYhGO6FgqJ7sA5T4w',
    localPath: "../images"
};

var levelSizes = getLevelSizes (options.maxLevel);

function getLevelSizes (maxLevel) {
    for (var j = 0, i = 1, result = {}; j <= maxLevel; j++) { result[j] = i; i = i * 2; }
    return result;
};

function generateTilesUrlList (opts) {

    for (var l = opts.minLevel, arr = []; l <= opts.maxLevel; l++) {
        var mapSize = +levelSizes[l];
        for (var x = 0; x < mapSize; x++) {
            for (var y = 0; y < mapSize; y++) {
                var url = [opts.url, '/', l, '/', x , '/', y, '.png', (opts.token || "")].join('');
                var local = [opts.localPath, '/', l, '/', x, '/', y, '.png'].join('');
                arr.push ({ url: url, local: local, l: l, x: x, y: y });
            }
        }
    }
    return arr;
}

function generateLocalDirs (opts) {

    if (!fs.existsSync(opts.localPath)) { fs.mkdirSync(opts.localPath); }

    for (var l = opts.minLevel; l <= opts.maxLevel; l++) {

        var path = [opts.localPath, '/', l].join('');
        if (!fs.existsSync(path)) { fs.mkdirSync(path); }

        for (var x = 0; x < levelSizes[l]; x++) {
            var path = [opts.localPath, '/', l, '/', x].join('');
            if (!fs.existsSync(path)) { fs.mkdirSync(path); }
        }
    }
}

var files = generateTilesUrlList (options);
console.log ('GENERATE TILES LIST [OK]');

generateLocalDirs(options);
console.log ('GENERATE FOLDERS STRUCTURE [OK]');


var counter = 0;
var workers = 0;

function download () {

    if (workers >= maxThreads) { return; }
    if (counter == files.length) { return; }

    workers++;
    var fileId = counter++;
    var fileName = files[fileId].url;
    var localFilename =  files[fileId].local;

    fetch (fileName)
        .then (function (res){
            workers--;

            if (fileId == files.length) { return; }

            var dest = fs.createWriteStream(localFilename);
            res.body.pipe(dest);
            console.log (fileName + ' [ downloaded ]');

            for (var j = 0; j < (maxThreads - workers); j++) { download(); }
        });
};

for (var i = 0; i < maxThreads; i++) { download (); }
